var express = require("express");
var router = express.Router();

/* GET users listing. */
const testUsers = [
  {
    id: 1,
    name: "Kenneth Mallari",
    email: "kenneth@mail.com",
    password: "1234",
  },
  {
    id: 2,
    name: "John Doe",
    email: "john@mail.com",
    password: "1234",
  },
  {
    id: 2,
    name: "Anne Hunter",
    email: "anne.hunter@mail.com",
    password: "1234",
  },
];

router.get("/", function (req, res, next) {
  res.send({ testUsers });
});

module.exports = router;
