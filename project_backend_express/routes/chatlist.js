var express = require("express");
var router = express.Router();

const { MongoClient, ObjectId } = require("mongodb");

var url = "mongodb://localhost:27017/";

const client = new MongoClient(url);
client.connect();
var dbo = client.db("project_dashboard");
const collection = dbo.collection("chats");

// GET all chats:
router.get("/", function (req, res, next) {
  collection.find({}).toArray(function (err, result) {
    if (err) throw err;
    res.json(result);
  });
});

// POST a Chat:
router.post("/", function (req, res, next) {
  collection.insertOne(
    {
      chatId: req.body.chatId,
      date: req.body.date,
      message: req.body.message,
      sender: req.body.sender,
    },
    function (err, result) {
      if (err) throw err;
      res.send(result);
    }
  );
});

module.exports = router;
