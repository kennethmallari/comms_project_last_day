# Communication Application

# Setup Guide

- Clone the project repository from BitBucket.
  ```bash
  git clone <project_url>
  ```
- Open the cloned project using an IDE. VS Code is preferred.
- `project` folder is for the frontend while `project_backend_express` folder is the backend.

## Backend:

- On your terminal, go to the `project_backend_express` directory and run:

```bash
  npm install
```

this will install the packages and the dependencies.

- To start the backend, run this command on your terminal:

```bash
  npm start
```

- If in case you encounter this error:

```bash
const utf8Encoder = new TextEncoder();
                    ^
ReferenceError: TextEncoder is not defined
```

Open your `encoding.js` folder in `node_modules` > `whatwg-url` > `dist` and replace these line of codes:

```bash
"use strict";
const utf8Encoder = new TextEncoder();
const utf8Decoder = new TextDecoder("utf-8", { ignoreBOM: true });
```

With these:

```bash
"use strict";
var util= require('util');
const utf8Encoder = new util.TextEncoder();
const utf8Decoder = new util.TextDecoder("utf-8", { ignoreBOM: true }
```

- Save the file, then try to run `npm start` again.

## Frontend:

- Open the `project` folder > `pages` folder then run `welcomePage.html` using the live server or by opening it on your browser.

---

## NOTE: I also included the zip file of the project with the node_modules. The filename is `comms_project.zip`. To use it instead:

- Extract the zip file.
- Open the extracted folder in an IDE, VS Code is preferred.

## Backend:

- To run the backend, go to your terminal and go to the `project_backend_express` directory.
- Run the command `npm start` to start the backend.

## Frontend:

- To run the Frontend, open the `project` folder > `pages` folder.
- Run the `welcomePage.html` using the live server or open it on your browser.

# Packages

[express ^4.17.1](https://www.npmjs.com/package/express)

- Fast, unopinionated, minimalist web framework for node.

[bcrypt ^5.0.1](https://www.npmjs.com/package/bcrypt)

- A library to help you hash passwords.

[cors 2.8.5](https://www.npmjs.com/package/cors)

- CORS is a node.js package for providing a Connect/Express middleware that can be used to enable CORS with various options.

[formidable ^2.0.1](https://www.npmjs.com/package/formidable)

- A Node.js module for parsing form data, especially file uploads.

[express-session ^1.17.2](https://www.npmjs.com/package/express-session)

- an HTTP server-side framework used to create and manage a session middleware.

[nodemon ^2.0.15](https://www.npmjs.com/package/nodemon)

- a tool that helps develop node.js based applications by automatically restarting the node application when file changes in the directory are detected.

[mongodb ^4.2.0](https://www.npmjs.com/package/mongodb)

- The official MongoDB driver for Node.js.

---

# Versions

- [Node 10.23.1](https://nodejs.org/en/blog/release/v10.23.1/)
